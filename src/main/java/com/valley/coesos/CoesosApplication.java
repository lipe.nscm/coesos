package com.valley.coesos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoesosApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoesosApplication.class, args);
	}

}
