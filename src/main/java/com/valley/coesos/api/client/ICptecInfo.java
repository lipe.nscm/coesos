package com.valley.coesos.api.client;

import org.springframework.stereotype.Service;

import com.valley.coesos.api.dto.Cidades;

import retrofit2.Call;
import retrofit2.http.GET;

@Service
public interface ICptecInfo {
	@GET("XML/listaCidades")
    Call<Cidades> listarCidades();

}
