package com.valley.coesos.api.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.valley.coesos.api.dto.Cidades;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

@Service
@SuppressWarnings("deprecation")
public class CptecInfoClient implements Callback<Cidades> {

    static final String BASE_URL = "http://servicos.cptec.inpe.br/";
    
        
    public Optional<Cidades> start() {
 
    	Cidades cidades = new Cidades();
    	cidades.setCidades(new ArrayList<>());
        @SuppressWarnings("deprecation")
		Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();

        ICptecInfo gerritAPI = retrofit.create(ICptecInfo.class);

        Call<Cidades> call = gerritAPI.listarCidades();
        try {
        	 cidades = 	call.execute().body();
        	 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return Optional.empty();
		}
        return Optional.of(cidades);
    }

	@Override
	public void onResponse(Call<Cidades> call, Response<Cidades> response) {
		if(response.isSuccessful()) {
            Cidades cidades= response.body();
            cidades.getCidades().forEach(x -> System.out.println(x.getNome()));
        } else {
            System.out.println(response.errorBody());
        }
		
	}

	@Override
	public void onFailure(Call<Cidades> call, Throwable t) {
		t.printStackTrace();
		
	}

}
