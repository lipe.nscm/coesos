package com.valley.coesos.api.dto;

import org.simpleframework.xml.Element;

@Element(name="cidade")
public class Cidade {

	@Element(name="nome")
	private String nome;
	@Element(name="uf")
	private String uf;
	@Element(name="id")
	private Integer id;
	

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}
