package com.valley.coesos.api.dto;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

@Element(name="cidades")
public class Cidades {

	@ElementList(inline = true, name="cidades")
	private List<Cidade>cidades;

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}
}
