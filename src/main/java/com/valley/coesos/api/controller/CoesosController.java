package com.valley.coesos.api.controller;

import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.valley.coesos.api.client.CptecInfoClient;
import com.valley.coesos.api.dto.Cidades;
import com.valley.coesos.api.response.Response;

@RestController
@RequestMapping("/")
@CrossOrigin(origins = "*")
public class CoesosController {

	@Autowired
	private CptecInfoClient client;

	@GetMapping("/")
	public ResponseEntity<Response<String>> listar() {

		Response<String> response = new Response<>();
		response.setData("Coesos - v.01");
		return ResponseEntity.ok(response);
	}

	@GetMapping("cidades/")
	public ResponseEntity<Response<String>> listarCidades() {
		Gson gson = new Gson();
		Response<String> response = new Response<>();
		Optional<Cidades> retorno = client.start();
		if (retorno.isPresent())
			response.setData(gson.toJson(retorno.get()));
		else {
			response.setData(null);
			response.setErrors(Arrays.asList("Falha na comunicação com CPTEC."));

		}

		return ResponseEntity.ok(response);
	}
}
